from abc import ABC, abstractclassmethod

class Person(ABC):
    def __init__(self, first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
    # Getter
    def getFirstName(self):
        return self._firstName

    def getLastName(self):
        return self._lastName

    def getEmail(self):
        return self._email

    def getDepartment(self):
        return self._department

    # Setter
    def setFirstName(self, firstName):
        self._firstName = firstName

    def setLastName(self, lastName):
        self._lastName = lastName

    def setEmail(self, email):
        self._email = email

    def setDepartment(self, department):
        self._department = department

    #methods
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        pass

    def addUser(self):
        pass

class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
        self._members = []
    # Getter
    def getFirstName(self):
        return self._firstName

    def getLastName(self):
        return self._lastName

    def getEmail(self):
        return self._email

    def getDepartment(self):
        return self._department

    def getMembers(self):
        return self._members
    # Setter
    def setFirstName(self, firstName):
        self._firstName = firstName

    def setLastName(self, lastName):
        self._lastName = lastName

    def setEmail(self, email):
        self._email = email

    def setDepartment(self, department):
        self._department = department

    def getMembers(self, members):
        self._members = members         
    #Methods   
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def get_members(self):
        return self._members

    def addMember(self, employee):
        self._members.append(employee)

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def addUser(self):
        pass

class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
    # Getter
    def getFirstName(self):
        return self._firstName

    def getLastName(self):
        return self._lastName

    def getEmail(self):
        return self._email

    def getDepartment(self):
        return self._department
    # Setter
    def setFirstName(self, firstName):
        self._firstName = firstName

    def setLastName(self, lastName):
        self._lastName = lastName

    def setEmail(self, email):
        self._email = email

    def setDepartment(self, department):
        self._department = department

    #methods    
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def addUser(self):
        return "User has been added"

class Request:
    def __init__(self, name, requester, date_requested):
        self._name = name
        self._requester = requester
        self._date_requested = date_requested
        self._status = "open"
    def getName(self):
        return self._name

    def getRequester(self):
        return self._requester

    def getDateRequested(self):
        return self._dateRequested

    def getStatus(self):
        return self._status     

    #setter
    def setName(self, name):
        self._name = name

    def setRequester(self, requester):
        self._requester = requester

    def setDateRequested(self, dateRequested):
        self._dateRequested = dateRequested

    def setStatus(self, status):
        self._status = status
    #Methods
    def updateRequest(self):
        self._status = "updated"
        return f"Request {self._name} has been updated"

    def closeRequest(self):
        self._status = "closed"
        return f"Request {self._name} has been closed"

    def cancelRequest(self):
        self._status = "cancelled"
        return f"Request {self._name} has been cancelled"

    def set_status(self, status):
        self._status = status
# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
 print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())                