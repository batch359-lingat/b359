#1.Accept a year input from the user and determine if it is a leap year or not.
year = int(input("Enter a year:"))

if year % 4 == 0 and year % 100 != 0:
	print(f"{year} is a leap year")
elif year < 0 or year == 0: # - No zero or negative values
	print(f"{year} please enter a positive year or greater than 0.")
else:
	print(f"{year} is not a leap year")		

# 2. Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

row = int(input(f"Enter number of rows :"))
col = int(input(f"Enter number of columns :"))

if row and col:
	rows = int(row)
	cols = int(col)
else:
	print("Error")

for i in range(rows):
	for j in range (cols):
		print("*", end = "")
	print()	
