# Input
# input () is simiilar to prompt() in JS that seeks to gather data from user input.
# \n stands for a line break
# username = input("Please enter your name: \n")
# print(f"Hi {username}! Welcome to Python Short Course")

# num1 = input("Enter 1st number: \n")
# num2 = input("Enter 2nd number: \n")
# print(f"The sum of num 1 and num 2 is {num1 + num2}")
# print(type(num1))
# print(type(num2))
# in JS = typeof
# in Python = type() 

# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))
# print(f"The sum of num 1 and num 2 is {num1 + num2}")


# If-Else statements
# Control Structures
	# 1. Selection
		# allow the program to choose



#If-Else Statements

# test_num =75

# if test_num >= 60:
# 	print("Test passed")
# else:
# 	print("Test failed")

# # If Else chains
# test_num2 = int(input("Please enter the 2nd test number \n"))

# if test_num2 > 0:
# 	print("The number is positive")
# elif test_num2 == 0:
# 	print("The number is zero")
# else:
# 	print("The number is negative")		

# The elif the shorthand for "else if" in other programming language

#Mini Exercise 1:
#create a if=else statement that determines if a number is divisible by 3, 5 or both
# if the numbebr is divisible by 3, print "The number is divisible by 3"
#If the number is divisible by 5, print "the numbebr is divisibble by 5"
# if the number is divbisible by 3 and 5 print "The number is divisible by both 3 and 5"
# If the number is NOT divisible by any, print "The number is not divisible by 3 or 5"

# test_num3 = int(input("Please enter the 3rd test number \n"))

# if test_num3 % 3 == 0 and test_num3 % 5 == 0:
# 	print("The number is divisible by both 3 and 5")
# elif test_num3 % 3 == 0:
# 	print("The number is divisible by 3")
# elif test_num3 % 5 == 0:
# 	print("The number is divisible by 5")
# else:
# 	print("The number is not divisible by 3 or 5")			

# test_div_num = int(input("Please enter a number: \n"))
# if test_div_num % 3 == 0 and test_div_num % 5 == 0:
# 	print("The number is divisible by both 3 and 5")
# elif test_div_num % 3:
# 	print("The number is divisible by  3")
# elif test_div_num % 5 == 0:
# 	print("The number is divisible by 5")	
# else:
# 	print("The number is not divisble by 3 or 5")

# Loops
# Python has loops that can repeat blocks of codes
# While Loops are used to execute a set of statements as long as the condition is true

# i = 1
# while i <= 5:
# 	print(f"The current count {i}")
# 	i += 1

# For Loops are used for iterating over a sequence
# fruits = ["apple", "banana", "cherry"]
# for indiv_fruit in fruits:
# 	print(indiv_fruit)

# range() method
# Syntax:
	# range(stop)
	# range(start, stop)
	# range(start, stop, step)

# for x in range(6):
# 	print(f"The current value is {x}")

# The range() functions defaults to 0 as a starting value. AS such, this prints the values from 0 to 5

# for x in range(6, 10):
# 	print(f"The current is {x}")	
# This priints the value from 6 to 9. always remember that the range always prints n-1

# for x in range(6, 20, 2):
# 	print(f"The current value is {x}")
# A third argument can be added to specify the increment of the loop.

# Mini exercise 2:
# Create a loop to count and display the number of even numbers between 1 and 20
# print "The number of even number between 1 and 20 is: <a total number of even numbers"

# count = 0

# for x in range(1, 21):
# 	if x % 2 == 0:
# 		count += 1

# print(f"The number of even numbers between 1 and 20 is : {count}")	

# Mini Exercise
#Write a python program that takes an iinteger input from tthe user and displays the multiiplaction table for that number from 1 to 10

# number = int(input("Enter an integer :"))
# print(f"Multiplication table for {number}")

# for i in range (1, 11):
# 	result = number * i
# 	print(f"{number} x {i} = {result}")

# Break statement - is used to stop the loop
# j = 1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j += 1

# Continue Statement - returns the control to the beginning of the while loop and continue with the next iteration
# k = 1
# while k < 6:
# 	k += 1
# 	if k == 3:
# 		continue
# 	print(k)
